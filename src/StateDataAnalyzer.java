package StateDataAnalyzer;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This program will analyze a table of U.S. state data (State_Data.csv) to find a variety of maximum/minimum results
 * according to the user's selection. A menu will be repeatedly offered as follows:
 * A. Find state with maximum land mass
 * B. Find state with minimum land mass
 * C. Find state with maximum population
 * D. Find state with minimum population
 * E. Find state with maximum average income
 * F. Find state with minimum average income
 *
 * Author:          John Reagan
 * Last Edit Date:  01/28/2021
 *
 * Algorithm -
 * 1. Display the intro message for the program to the user. This program will analyze a table of U.S. state data
 *      (State_Data.csv) to find a variety of maximum/minimum results according to the user's selection.
 * 2. Use a 'keyboard' Scanner object to get the user’s menu selection (A-F). If the captured input is not between A-F,
 *      an error message will display and the program will exit.
 * 3. Based on the user’s input, use the findMaxIndex() or findMinIndex() methods on the three arrays of state data
 *      captured from the input file.
 * 4. With the returned index value from the previous method call, output the applicable stateName[index] string and the
 *      corresponding data value requested by the user.
 * 5. Re-prompt the user with a do-while loop until an input value causes the program to exit.
 */

public class StateDataAnalyzer {
    private static int NUMBER_STATES = 50;

    /**
     * Load state names from the file
     * @param fileName      Input file.
     * @return array of strings with the state names
     * @throws Exception    For problems with input file.
     */
    public static String[] loadNamesFromFile(String fileName) throws Exception {
        File file = new File(fileName);
        String[] array = new String[NUMBER_STATES];

        Scanner fileScan = new Scanner(file);
        for(int row = 0; row < NUMBER_STATES; row++)
        {
            array[row] = fileScan.next();
            fileScan.nextLine();
        }
        return array;
    }

    /**
     * Load the per capita income for each state
     * @param fileName      Input file.
     * @return array of doubles with the state per capita income
     * @throws Exception    For problems with input file.
     */
    public static double[] loadPerCapitaFromFile(String fileName) throws Exception {
        File file = new File(fileName);
        double[] array = new double[NUMBER_STATES];

        Scanner fileScan = new Scanner(file);
        for(int row = 0; row < NUMBER_STATES; row++)
        {   fileScan.next();
            array[row] = fileScan.nextDouble();
            fileScan.nextLine();
        }
        return array;
    }

    /**
     * Load the state population from a file
     * @param fileName      Input file.
     * @return array of integers with each state population
     * @throws Exception    For problems with input file.
     */
    public static int[] loadPopulationFromFile(String fileName) throws Exception {
        File file = new File(fileName);
        int[] array = new int[NUMBER_STATES];

        Scanner fileScan = new Scanner(file);
        for(int row = 0; row < NUMBER_STATES; row++)
        {   fileScan.next();
            fileScan.next();
            array[row] = fileScan.nextInt();
            fileScan.nextLine();
        }
        return array;
    }

    /**
     * Load the land size from a file
     * @param fileName      Input file.
     * @return array of doubles with each states land size
     * @throws Exception    For problems with input file.
     */
    public static double[] loadLandSizeFromFile(String fileName) throws Exception {
        File file = new File(fileName);
        double[] array = new double[NUMBER_STATES];

        Scanner fileScan = new Scanner(file);
        for(int row = 0; row < NUMBER_STATES; row++)
        {   fileScan.next();
            fileScan.next();
            fileScan.next();
            array[row] = fileScan.nextDouble();
        }
        return array;
    }

    /**
     * @param args
     * @throws Exception    For problems with input file.
     */
    public static void main(String[] args) throws Exception {
        String fileName = "State_Data.csv", menu, input, output;
        String[] stateNames    = loadNamesFromFile(fileName);
        double[] perCapitaData = loadPerCapitaFromFile(fileName);
        int[] populationData   = loadPopulationFromFile(fileName);
        double[] landSizeData  = loadLandSizeFromFile(fileName);
        int index = 0;

        // Convert populationData to array of doubles for find[Max/Min]Index methods.
        double[] popData = new double[populationData.length];
        for (int i=0; i < populationData.length; i++) {
            popData[i] = (double)populationData[i];
        }

        // Call the INTRO method and build the menu string to be displayed.
        displayIntro();
        menu = "A - Find state with maximum land mass\nB - Find state with minimum land mass" +
                "\nC - Find state with maximum population\nD - Find state with minimum population" +
                "\nE - Find state with maximum average income\nF - Find state with minimum average income" +
                "\n\nAny input other than the menu selection A-F will cause the program to quit and exit.";

        // Create program loop with menu.
        do {
            input = JOptionPane.showInputDialog(null, menu, "StateDataAnalyzer by John Reagan",
                    JOptionPane.QUESTION_MESSAGE);
            char letter = input.toUpperCase().charAt(0);

            // Clear the output string and call the applicable methods based on the user's input.
            output = "";
            switch (letter) {
                case 'A':
                    index = findMaxIndex(landSizeData);
                    output = String.format("A - The state with the MAXIMUM Land Mass is: %s", stateNames[index]);
                    break;
                case 'B':
                    index = findMinIndex(landSizeData);
                    output = String.format("B - The state with the MINIMUM Land Mass is: %s", stateNames[index]);
                    break;
                case 'C':
                    index = findMaxIndex(popData);
                    output = String.format("C - The state with the MAXIMUM Population is: %s", stateNames[index]);
                    break;
                case 'D':
                    index = findMinIndex(popData);
                    output = String.format("D - The state with the MINIMUM Population is: %s", stateNames[index]);
                    break;
                case 'E':
                    index = findMaxIndex(perCapitaData);
                    output = String.format("E - The state with the MAXIMUM Average Income is: %s", stateNames[index]);
                    break;
                case 'F':
                    index = findMinIndex(perCapitaData);
                    output = String.format("F - The state with the MINIMUM Average Income is: %s", stateNames[index]);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, letter + " isn't between A-F of the " +
                            "menu. Program will now exit.", "StateDataAnalyzer Exit", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
            }
            JOptionPane.showMessageDialog(null, output +
                    stateData(landSizeData[index], populationData[index], perCapitaData[index]),
                    "StateDataAnalyzer Output", JOptionPane.INFORMATION_MESSAGE);
        } while (true);
    }

    /**
     * Displays the introduction statement.
     * @throws Exception        For problems with input file.
     */
    public static void displayIntro() throws Exception {
        String intro = "This program will display state data to the user based on menu input. You should enter the\n " +
                "letter corresponding to the task you would like to do and then click on OK. The program\n " +
                "will always return to the menu.";
        JOptionPane.showMessageDialog(null, intro, "StateDataAnalyzer Introduction",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Find the max index for the passed array of doubles
     * @param array         Find the max value of the input array.
     * @return maxIndex     The index of the max value in the array.
     */
    public static int findMaxIndex(double[] array) {
        double highest = array[0];
        int maxIndex = 0;
        for (int j = 1; j < array.length; j++) {
            if (array[j] > highest) {
                highest = array[j];
                maxIndex = j;
            }
        }
        return maxIndex;
    }

    /**
     * Find the min index for the passed array of doubles
     * @param array         Find the min value of the input array.
     * @return minIndex     The index of the min value in the array.
     */
    public static int findMinIndex(double[] array) {
        double lowest = array[0];
        int minIndex = 0;
        for (int k = 1; k < array.length; k++) {
            if (array[k] < lowest) {
                lowest = array[k];
                minIndex = k;
            }
        }
        return minIndex;
    }

    /**
     * Construct a state's data string for the param 'stateIndex'.
     * @param landSize in sq mi, .00 precision
     * @param population, comma separated
     * @param perCapita in $USD, .00 precision
     * @return output   The string to display in JOptionPane.
     */
    public static String stateData(double landSize, int population, double perCapita) {
        String output = String.format("\n-- land mass = %,.2f sq mi", landSize);
        output += String.format("\n-- population = %,d people", population);
        output += String.format("\n-- per capita income = $%,.2f", perCapita);
        return output;
    }
}
